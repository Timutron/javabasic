/*
  Given 2 int values greater than 0, return whichever value is nearest to 21 without going over.
  Return 0 if they both go over.
*/

public class Logic2blackjack {
    public int blackjack(int a, int b) {
        int sum = 21;
        if (a <= sum && b <= sum) {
            int one = sum > a ? (sum - a) : (a - sum);
            int two = sum > b ? (sum - b) : (b - sum);
            return one < two ? a : b;
        } else if (a <= sum && b > sum)
            return a;
        else if (a > sum && b <= sum)
            return b;
        else return 0;
    }
}
