
/*
  Given 3 int values, a b c, return their sum.
  However,if one of the values is 13 then it does not count towards the sum and values to its right do not count.
  So for example, if b is 13, then both b and c do not count.
*/

public class Logic2luckySum {
    public int luckySum(int a, int b, int c) {
        int del = 13;
        if (a == del)
            return 0;
        else if (b == del)
            return a;
        else if (c == del)
            return a + b;
        else
            return a+b+c;
    }
}
