
/*
 We want make a package of goal kilos of chocolate. We have small bars (1 kilo each) and big bars (5 kilos each).
 Return the number of small bars to use, assuming we always use big bars before small bars. Return -1 if it can't be done.
*/

public class Logic2makeChocolate {
    public int makeChocolate(int small, int big, int goal) {
        int BigUse = goal/5;
        int n = 0;
        if (BigUse > big)
            n = BigUse - big;
        int smallUse = goal%5 + (n * 5);
        if (smallUse == 0)
            return 0;
        if (smallUse <= small)
            return smallUse;
        else return -1;
        }
}
